from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

# Create your views here.
class CreateUserView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("home")
    template_name = "registration/signup.html"

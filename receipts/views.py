from django.views.generic.list import ListView
from django.shortcuts import redirect, render
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        form.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categorylist.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accountlist.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/newexpense.html"
    fields = ["name"]

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.save()
        return redirect("categories_list")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/newaccount.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.save()
        return redirect("accounts_list")

from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
    ExpenseCreateView,
    AccountCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="categories_list",
    ),
    path(
        "accounts/",
        AccountListView.as_view(),
        name="accounts_list",
    ),
    path(
        "categories/create/",
        ExpenseCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="create_account",
    ),
]
